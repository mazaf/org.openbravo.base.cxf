/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class contains the logic to register SOAP web services.
 */
@ApplicationScoped
public class EndpointPublisher {

  private static final Logger log = LogManager.getLogger();

  @Inject
  @Any
  private Instance<SOAPWebServiceRegister> registers;

  void publishSoapWebServices() {
    registers.stream().forEach(this::publishSoapWebServices);
  }

  private void publishSoapWebServices(SOAPWebServiceRegister register) {
    register.getSOAPWebServices()
        .stream()
        .map(this::publish)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(Endpoint::getInInterceptors)
        .forEach(inInterceptors -> {
          // Add the interceptors provided by the SOAPWebServiceRegister
          inInterceptors.addAll(register.getInterceptors());

          // The base interceptor should always be present in the chain
          inInterceptors.add(new BaseInInterceptor());
        });
  }

  private Optional<Endpoint> publish(SOAPWebService<?> service) {
    try {
      String endpointAddress = getEndpointAddress(service.getAddress());
      javax.xml.ws.Endpoint endpoint = javax.xml.ws.Endpoint.publish(endpointAddress,
          service.getImplementor());
      if (!(endpoint instanceof EndpointImpl)) {
        log.error("SOAP web service {} not published through CXF", service.getAddress());
        return Optional.empty();
      }
      return Optional.of(((EndpointImpl) endpoint).getServer().getEndpoint());
    } catch (Exception ex) {
      log.error("Could not publish SOAP web service {}", service.getAddress(), ex);
      return Optional.empty();
    }
  }

  private String getEndpointAddress(String endpointAddress) {
    return endpointAddress.startsWith("/") ? endpointAddress : "/" + endpointAddress;
  }

}
