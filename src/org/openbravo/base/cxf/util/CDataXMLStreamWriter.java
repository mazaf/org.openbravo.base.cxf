/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.util;

import java.util.Set;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.cxf.staxutils.DelegatingXMLStreamWriter;

/**
 * An XML Stream Writer that exports some elements as CDATA. It allows to be configured to delete
 * specific parts of the XML content.
 */
class CDataXMLStreamWriter extends DelegatingXMLStreamWriter {

  private final Set<String> cdataElements;
  private final Set<String> contentToRemove;
  private String currentElementName;

  CDataXMLStreamWriter(XMLStreamWriter writer, Set<String> cdataElements,
      Set<String> contentToRemove) {
    super(writer);
    this.cdataElements = cdataElements;
    this.contentToRemove = contentToRemove;
  }

  @Override
  public void writeStartElement(String prefix, String local, String uri) throws XMLStreamException {
    currentElementName = local;
    super.writeStartElement(prefix, local, uri);
  }

  @Override
  public void writeCharacters(String text) throws XMLStreamException {
    if (cdataElements.contains(currentElementName)) {
      super.writeCData(cleanText(text));
    } else {
      super.writeCharacters(text);
    }
  }

  private String cleanText(String text) {
    return contentToRemove.stream().reduce(text, (result, element) -> result.replace(element, ""));
  }
}
