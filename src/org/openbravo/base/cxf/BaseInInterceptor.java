/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf;

import org.apache.cxf.interceptor.AbstractInDatabindingInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openbravo.authentication.AuthenticationException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.SessionInfo;

/**
 * An incoming interceptor that takes care of different base tasks:
 * 
 * - Checks if the user is accessing to the web service with a role enabled to use web services.<br>
 * - Sets the audit information.<br>
 * - Rollbacks the DAL transaction in case an error happens during the execution of the SOAP web
 * service.
 */
public class BaseInInterceptor extends AbstractInDatabindingInterceptor {
  private static final Logger log = LogManager.getLogger();
  private static final String WS_BASE_PATH = "org.apache.cxf.message.Message.BASE_PATH";
  private static final String PROCESS_TYPE = "WS";
  private static final String DEFAULT_SERVICE_NAME = "CXF";
  private static final int PROCESS_ID_MAX_LENGTH = 32;

  public BaseInInterceptor() {
    super(Phase.RECEIVE);
  }

  @Override
  public void handleMessage(Message message) throws Fault {
    String userId = null;
    String serviceName = getServiceName(message);
    if (OBContext.getOBContext() != null) {
      checkWebServicesEnabled(serviceName);

      userId = OBContext.getOBContext().getUser().getId();
      SessionInfo.setUserId(userId);
    }

    SessionInfo.setProcessType(PROCESS_TYPE);
    SessionInfo.setProcessId(serviceName);

    log.trace("SOAP WS {} accessed by user with ID {}", serviceName, userId);
  }

  private void checkWebServicesEnabled(String serviceName) {
    if (OBContext.getOBContext().isWebServiceEnabled()) {
      return;
    }
    log.error("User {} with role {} is trying to access to non granted SOAP web service {}",
        OBContext.getOBContext().getUser(), OBContext.getOBContext().getRole(), serviceName);
    throw new AuthenticationException(
        "Web Services are not granted to " + OBContext.getOBContext().getRole() + " role");
  }

  private String getServiceName(Message message) {
    try {
      String wsBasePath = (String) message.getContextualProperty(WS_BASE_PATH);
      String serviceName = wsBasePath.substring(wsBasePath.lastIndexOf('/') + 1);
      if (serviceName.length() > PROCESS_ID_MAX_LENGTH) {
        serviceName = serviceName.substring(0, PROCESS_ID_MAX_LENGTH);
      }
      return serviceName;
    } catch (Exception ex) {
      return DEFAULT_SERVICE_NAME;
    }
  }

  @Override
  public void handleFault(Message message) {
    Exception e = message.getContent(Exception.class);
    // In case of an AuthenticationException do not rollback. We need to save the ad_session
    // information in DB in order to properly count the WS calls
    if (!(e instanceof AuthenticationException)) {
      OBDal.getInstance().rollbackAndClose();
    }
  }
}
