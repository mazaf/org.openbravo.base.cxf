/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.security;

import java.net.HttpURLConnection;

/**
 * Class intended to handle the HTTP 403 (Forbidden) error.
 */
class HttpForbiddenException extends HttpAuthenticationException {

  private static final long serialVersionUID = 1L;
  private static final String INVALID_CREDENTIALS = "Invalid Credentials";

  public HttpForbiddenException() {
    super(HttpURLConnection.HTTP_FORBIDDEN);
  }

  @Override
  public String getMessage() {
    return INVALID_CREDENTIALS;
  }
}
