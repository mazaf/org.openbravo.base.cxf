/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.security;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.LoginUtils;
import org.openbravo.service.db.DalConnectionProvider;

/**
 * This class can be used to validate the basic authorization data included in an HTTP request.
 */
@ApplicationScoped
public class HttpBasicAuthenticator {

  /**
   * Retrieves the ID of the user that matches the authorization data included in an HTTP request.
   * 
   * @param request
   *          An HttpServletRequest with the authorization data.
   * @return an Optional with the ID of the user that matches the authorization data included in the
   *         provided HttpServletRequest. If no user is found then an empty Optional is returned.
   */
  public Optional<String> authenticate(HttpServletRequest request) {
    AuthenticationData data = decodeBasicAuthenticationData(request);
    if (data == null) {
      return Optional.empty();
    }
    return authenticate(data.user, data.password);
  }

  /**
   * Retrieves the ID of the user that matches the provided login and password.
   * 
   * @param user
   *          The user name
   * @param password
   *          The user's password
   * 
   * @return an Optional with the ID of the user that matches the provided user name and password.
   *         If no user is found then an empty Optional is returned.
   */
  private Optional<String> authenticate(String user, String password) {
    String userId = LoginUtils.getValidUserId(new DalConnectionProvider(false), user, password);
    return Optional.ofNullable(userId);
  }

  private AuthenticationData decodeBasicAuthenticationData(HttpServletRequest request) {
    try {
      final String auth = request.getHeader("Authorization");
      if (auth == null) {
        return null;
      }
      if (!auth.toUpperCase().startsWith("BASIC ")) {
        return null; // only BASIC supported
      }

      // user and password come after BASIC
      final String userpassEncoded = auth.substring(6);

      // Decode it, using any base 64 decoder
      final String decodedUserPass = new String(Base64.decodeBase64(userpassEncoded.getBytes()));
      final int index = decodedUserPass.indexOf(':');
      if (index == -1) {
        return null;
      }

      String user = decodedUserPass.substring(0, index);
      String password = decodedUserPass.substring(index + 1);
      return new AuthenticationData(user, password);
    } catch (final Exception e) {
      throw new OBException(e);
    }
  }

  private static class AuthenticationData {
    private final String user;
    private final String password;

    AuthenticationData(String user, String password) {
      this.user = user;
      this.password = password;
    }
  }

}
